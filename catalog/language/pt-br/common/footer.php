<?php
// Text
$_['text_information']  = 'Informações';
$_['text_service']      = 'Serviços ao cliente';
$_['text_extra']        = 'Outros serviços';
$_['text_contact']      = 'FALE CONOSCO';
$_['text_return']       = 'Solicitar devolução';
$_['text_sitemap']      = 'Mapa do site';
$_['text_manufacturer'] = 'Produtos por marca';
$_['text_voucher']      = 'Comprar vale presentes';
$_['text_affiliate']    = 'Programa de afiliados';
$_['text_special']      = 'Produtos em promoção';
$_['text_account']      = 'Minha conta';
$_['text_order']        = 'Histórico de pedidos';
$_['text_wishlist']     = 'Lista de desejos';
$_['text_newsletter']   = 'Informativo';
$_['text_powered']      = 'Desenvolvido com tecnologia <a href="https://www.orions.com.br">Orions</a><br /> %s &copy; %s';
$_['text_whatsapp']     = 'Whatsapp:';
$_['text_telefone']     = 'Telefone:';
$_['text_email']        = 'Email:';
$_['text_facebook']     = 'Facebook';
$_['text_instagram']    = 'Instagram';
$_['text_youtube']      = 'Youtube';
$_['text_blog']         = 'Blog';
$_['text_twitter']      = 'Twitter';
