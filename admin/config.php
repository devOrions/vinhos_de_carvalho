<?php
// HTTP
define('HTTP_SERVER', 'http://dev.vinhosdecarvalho.com.br/admin/');
define('HTTP_CATALOG', 'http://dev.vinhosdecarvalho.com.br/');

// HTTPS
define('HTTPS_SERVER', 'http://dev.vinhosdecarvalho.com.br/admin/');
define('HTTPS_CATALOG', 'http://dev.vinhosdecarvalho.com.br/');

// DIR
define('DIR_APPLICATION', 'C:/wamp64/www/vinhosdecarvalho/admin/');
define('DIR_SYSTEM', 'C:/wamp64/www/vinhosdecarvalho/system/');
define('DIR_IMAGE', 'C:/wamp64/www/vinhosdecarvalho/image/');
define('DIR_STORAGE', 'C:/wamp64/www/storage/');
define('DIR_CATALOG', 'C:/wamp64/www/vinhosdecarvalho/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'vinhosdecarvalho');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
